-- Retrieve all sales in a specific month
SELECT 
  TO_CHAR(sale_date, 'YYYY-MM') AS year_month,
  COUNT(*) AS month_sales_count
FROM 
  sales_data
GROUP BY year_month
ORDER BY year_month;



-- Calculate the total sale_amount for each month
SELECT 
  TO_CHAR(sale_date, 'YYYY-MM') AS year_month, 
  SUM(sale_amount) AS total_amount
FROM
  sales_data
GROUP BY year_month
ORDER BY year_month;



-- Identify the top three salesperson_id values by sale_amount within a specific region across all partitions.
WITH person_sale AS (
    SELECT 
        region_id,
        salesperson_id,
        SUM(sale_amount) AS total_amount,
        RANK() OVER (PARTITION BY region_id ORDER BY SUM(sale_amount) DESC) AS person_rank
    FROM 
        sales_data
    GROUP BY 
        region_id, salesperson_id
)
SELECT 
    region_id,
    salesperson_id,
    total_amount
FROM 
     person_sale
WHERE 
    person_rank <= 3;
