-- Define a maintenance task to drop partitions older than 12 months and create new partitions for the next month.
CREATE OR REPLACE PROCEDURE manage_partitions()
LANGUAGE plpgsql
AS $$
DECLARE
    current_date DATE := CURRENT_DATE;
    last_year_date DATE := current_date - INTERVAL '1 year';
    partition_date DATE;
    partition_name VARCHAR;
    partition_start DATE;
    partition_end DATE;
BEGIN
    -- Drop partitions older than 12 months
    FOR counter IN 0..11 LOOP
        partition_date := last_year_date - (INTERVAL '1 month' * counter);
        partition_name := 'sales_data_' || TO_CHAR(partition_date, 'YYYY_MM');
        IF TO_REGCLASS(partition_name) IS NOT NULL THEN
            EXECUTE FORMAT('DROP TABLE %I', partition_name);
            RAISE NOTICE 'Dropped partition: %', partition_name;
        ELSE
            RAISE NOTICE 'Partition % does not exist, skipping drop.', partition_name;
        END IF;
    END LOOP;

    -- Create partitions for the next 12 months
    FOR counter IN 0..11 LOOP
        partition_start := DATE_TRUNC('month', current_date) + (INTERVAL '1 month' * counter);
        partition_end := partition_start + INTERVAL '1 month';
        partition_name := 'sales_data_' || TO_CHAR(partition_start, 'YYYY_MM');
        IF TO_REGCLASS(partition_name) IS NULL THEN
            EXECUTE FORMAT(
                'CREATE TABLE %I PARTITION OF sales_data FOR VALUES FROM (%L) TO (%L)', 
                partition_name, partition_start, partition_end
            );
            RAISE NOTICE 'Created partition: %', partition_name;
        ELSE
            RAISE NOTICE 'Partition % already exists, skipping creation.', partition_name;
        END IF;
    END LOOP;
END;
$$;

CALL manage_partitions();
